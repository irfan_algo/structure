import React from "react";

const Button = ({
  color,
  text,
  onClick,
  maxWidth,
  backgroundColor,
  padding,
  disabled,
  className,
  icon,
}) => {
  return (
    <button
      disabled={disabled}
      className={`baseline flex space-x-2 items-center p-3 px-6 pt-2 CustomButton ${!disabled && "btnHoverEffectOutline"} ${
        className ? className : " "
      }`}
      onClick={onClick}
      style={{
        color: color,
        backgroundColor: backgroundColor,
        maxWidth: maxWidth,
        padding: padding,
      }}
    >
      {icon &&  <div className="btnIcon">{icon}</div>}
      <h6>{text}</h6>
     
    </button>
  );
};

export default Button;
