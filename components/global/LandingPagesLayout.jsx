import React from "react";
import Head from "next/head";
import Navbar from "./Navbar";
import Footer from "./Footer";
import { useRouter } from "next/router";

const LandingPagesLayout = ({ children }) => {
  const Paths = ["", "/", "/components"];
  const router = useRouter();
  let showHeaderFooter = Paths.includes(router.pathname);

  return (
    <>
      <Head>
        <title>Structure</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        ></meta>
      </Head>
      {showHeaderFooter && <Navbar />}
      <div
        id={"layoutContainer"}
        onContextMenu={() => {
          return false;
        }}
      >
        {children}
      </div>
        {showHeaderFooter && <Footer />}

    </>
  );
};

export default LandingPagesLayout;
