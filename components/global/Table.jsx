import React from "react";

const Table = ({ tableHead, tableData }) => {
  return (
    <div className="overflow-x-auto relative customScrollOntables">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 ">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            {tableHead.map((item, index) => (
              <th key={index} scope="col" className="py-3 px-6">
                {item}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>{tableData}</tbody>
      </table>
    </div>
  );
};

export default Table;
