import React from "react";
import Image from "next/image";
import { useEffect } from "react";
import Button from "@/components/global/Button";
import { TickIcon } from "@/components/global/SVGAssets";
import Link from "next/link";
import { useRouter } from "next/router";
const Navbar = () => {
  const router = useRouter();
  useEffect(() => {
    const btn = document.getElementById("menu-btn");
    const nav = document.getElementById("menu");
    btn.addEventListener("click", (e) => {
      e.preventDefault();
      if (btn.classList.contains("open")) {
        btn.classList.remove("open");
      } else {
        btn.classList.toggle("open");
      }
      nav.classList.toggle("flex");
      nav.classList.toggle("hidden");
    });
  },[]);

  return (
    <nav className="relative container mx-auto p-6">
      {/* Flex container */}
      <div className="flex items-center justify-between">
        {/* Logo */}
        <div className="pt-2 flex items-center">
          <img
            src="https://flowbite.com/docs/images/logo.svg"
            className="mr-3 h-6 sm:h-9"
            alt="Flowbite Logo"
          />
          <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
            TestLogo
          </span>
        </div>
        {/* Menu Items */}
        <div className="hidden space-x-6 tablet:flex">
          <li
            className={`${
              router.pathname == "/" ? "text-gray-700" : "text-gray-400"
            } hover:text-gray-300 list-none`}
          >
            <Link href="/">Home</Link>
          </li>
          <li
            className={`${
              router.pathname == "/components"
                ? "text-gray-700"
                : "text-gray-400"
            } hover:text-gray-500 list-none`}
          >
            <Link href="/components">Components</Link>
          </li>
        </div>
        {/* Button */}
        <Button
          text="Get Started"
          className={
            "hidden tablet:flex text-white  rounded-full   bg-brightRed hover:bg-brightRedLight"
          }
          icon={<TickIcon />}
        />

        {/* Hamburger Icon */}
        <button
          id="menu-btn"
          className="block hamburger tablet:hidden focus:outline-none"
        >
          <span className="hamburger-top"></span>
          <span className="hamburger-middle"></span>
          <span className="hamburger-bottom"></span>
        </button>
      </div>

      {/* Mobile Menu */}
      <div className="tablet:hidden">
        <div
          id="menu"
          className="absolute flex-col items-center hidden self-end py-8 mt-10 space-y-6 font-bold bg-white sm:w-auto sm:self-center left-6 right-6 drop-shadow-md"
        >
          <li
            className={`${
              router.pathname == "/" ? "text-gray-700" : "text-gray-400"
            } hover:text-gray-300 list-none`}
          >
            <Link href="/">Home</Link>
          </li>
          <li
            className={`${
              router.pathname == "/components"
                ? "text-gray-700"
                : "text-gray-400"
            } hover:text-gray-500 list-none`}
          >
            <Link href="/components">Components</Link>
          </li>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
