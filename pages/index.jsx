import Head from "next/head";
import Image from "next/image";
import { robot } from "./assets";
import Button from "@/components/global/Button";
import { TickIcon } from "@/components/global/SVGAssets";
  
export default function Home({title="Structure", description="Basic Structure", imagelink="abcd.png"}) {
  return (
    <div>
  <Head>
            <title>{title}</title>
            <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"/>
            <meta name="description" content={description}/>
            <meta name="keywords" content="Structure Platform , nft market place , nethernft login, Structure Platform"/>
            <meta name="author" content="Structure Platform Platform."/>
            <meta name="robots" content="all,follow"/>
            <meta name="google" content="notranslate" />
            <meta name="theme-color" content="#000000"/>
            <meta name="msapplication-navbutton-color" content="#000000"/>
            <meta name="apple-mobile-web-app-status-bar-style" content="#000000"/>
            <meta name="msapplication-TileColor" content="#000000"/>
            
            <meta property="og:type" content="website" />
            <meta property="og:title" content={title} />
            <meta property="og:description" content={description} />
            <meta property="og:url" content="https://abcd.io/" />
            <meta property="og:site_name" content="Nether Platform" />
            <meta property="og:image" content={imagelink} />
            <meta property="og:image:width" content="320" />
            <meta property="og:image:height" content="320" />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:description" content={description} />
            <meta name="twitter:title" content={title} />
            <meta name="twitter:image" content={imagelink} />
            <meta name="google-site-verification" content="" />

            <link rel="apple-touch-icon" sizes="180x180" href={imagelink}/>
            <link rel="icon" type="image/png" sizes="32x32" href={imagelink}/>
            <link rel="icon" type="image/png" sizes="16x16" href={imagelink}/>
            <link rel="manifest" href="manifest.json" />
            <link rel="mask-icon" href={imagelink} color="#b59a5a"></link>
        </Head>


      <main>
        <section
          id="hero"
          className="min-h-[80vh] flex items-center justify-center"
        >
          <div className="container flex flex-col-reverse items-center px-6 mx-auto mt-10 space-y-0 tablet:space-y-0 tablet:flex-row">
            <div className="flex flex-col mb-32 space-y-12 tablet:w-1/2">
              <h1 className="max-w-tablet  font-bold text-center  tablet:text-left FontSizetitle">
                Bring everyone together to build better products
              </h1>
              <p className="max-w-lg text-center tablet:text-left FontSizeParagraph">
                Manage makes it simple for software teams to plan day-to-day
                tasks while keeping the larger team goals in view.
              </p>
              <div className="flex justify-center tablet:justify-start">
                <Button
                  text="Get Started"
                  // backgroundColor="#012A38"
                  // maxWidth="230px"
                  className={
                    " text-white  rounded-full   bg-brightRed hover:bg-brightRedLight"
                  }
                  icon={<TickIcon />}
                />
              </div>
            </div>
            <div className="tablet:w-1/2">
              <Image
                layout="responsive"
                objectFit="contain"
                width={526}
                height={526}
                src="/images/robot.png"
                alt={"3D art"}
              />
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}
