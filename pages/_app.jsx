import '../styles/global.css';
import '../styles/customstyle/customStyling.scss';
import React from "react";
import LandingPagesLayout from "@/components/global/LandingPagesLayout";
import { useRouter } from "next/router";


function MyApp({ Component, pageProps }) {
  const Paths = ["", "/", "/components"];
  const router = useRouter();
  let showHeaderFooter = Paths.includes(router.pathname);
  return (
    <React.Fragment>
      {showHeaderFooter && (
        <LandingPagesLayout>
          <Component {...pageProps} />
        </LandingPagesLayout>
      )}
      {!showHeaderFooter && (
        <div className="wrongUrl" style={{display : 'flex', justifyContent : 'center', alignItems : 'center', height: '100vh'}}>
          <h1>Wrong URL</h1>
        </div>
      )}
    </React.Fragment>
  );
}

export default MyApp;
