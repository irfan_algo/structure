import React, { useState, useEffect } from "react";
import Button from "@/components/global/Button";
import { TickIcon, EditCircleIcon } from "@/components/global/SVGAssets";
import Modal from "@/components/global/Modal";
import Table from "@/components/global/Table";
const components = () => {
  const [showModal, setShowModal] = React.useState(false);
  const [modaltitle, setmodaltitle] = useState("");
  const [modalcontent, setmodalcontent] = useState(<></>);
  const [modalfooter, setmodalfooter] = useState(<></>);
  const [tableHead, setTableHead] = useState([]);
  const [tableData, setTableData] = useState([]);

  const ModalFunction = () => {
    setmodaltitle("Create post");
    setmodalcontent(
      <div className="relative p-6 flex-auto">
        <p className="my-4 text-slate-500 text-lg leading-relaxed">
          I always felt like I could do anything. That’s the main thing people
          are controlled by! Thoughts- their perception of themselves! They're
          slowed down by their perception of themselves. If you're taught you
          can’t do anything, you won’t do anything. I was taught I could do
          everything.
        </p>
      </div>
    );
    setmodalfooter(
      <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
        <button
          className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
          type="button"
          onClick={() => setShowModal(false)}
        >
          Close
        </button>
        <button
          className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
          type="button"
          onClick={() => setShowModal(false)}
        >
          Save Changes
        </button>
      </div>
    );
    setShowModal(true);
  };
  const closeModal = () => {
    setShowModal(false);
  };
  const TableList = [
    {
      id:1,
      Name: "Apple MacBook Pro 17",
      Color: "Sliver",
      Category: "Laptop",
      Price: "$2999",
    },
    {
      id:2,
      Name: "Dell MacBook Pro 17",
      Color: "Sliver",
      Category: "Laptop",
      Price: "$4999",
    },
  ];
  const updatingTable = async() => {
    setTableHead(["Name", "Color", "Category", "Price", "Action"]);
    const tableData = await TableList.map((data) => {
      return (
        <tr key={data.id}className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
          <th
            scope="row"
            className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
          >
            {data?.Name}
          </th>
          <td className="py-4 px-6">{data?.Color}</td>
          <td className="py-4 px-6">{data?.Category}</td>
          <td className="py-4 px-6">{data?.Price}</td>
          <td className="py-4 px-6 flex items-center space-x-4">
            <button>
              <EditCircleIcon />
            </button>
            <Button
              text="Delete"
              className={
                "flex text-white  rounded-full  bg-brightRed hover:bg-brightRedLight"
              }
            />
          </td>
        </tr>
      );
    });
    setTableData(tableData);
  };
  useEffect(() => {
    updatingTable();
  }, []);
  return (
    <div className="container mx-auto p-6">
      <h1 className="FontSizetitle">Buttons</h1>
      <div className="flex justify-start items-center space-x-5 py-6">
        <Button
          text="Button1"
          className={
            "flex text-white  rounded-full  bg-brightRed hover:bg-brightRedLight"
          }
          icon={<TickIcon />}
        />
        <Button
          text="Button2"
          className={
            "flex text-white  rounded-full  bg-brightRed hover:bg-brightRedLight"
          }
        />
      </div>
      <h1 className="FontSizetitle">Modal</h1>
      <div className="flex justify-start items-center space-x-5 py-6">
        <Button
          text="Open Modal"
          className={
            "flex text-white  rounded-full   bg-brightRed hover:bg-brightRedLight"
          }
          icon={<TickIcon />}
          onClick={ModalFunction}
        />
      </div>
      {showModal ? (
        <Modal
          modalTitle={modaltitle}
          modalContent={modalcontent}
          modalFooter={modalfooter}
          closeModal={closeModal}
        />
      ) : null}

      <h1 className="FontSizetitle">Table</h1>
      <Table tableHead={tableHead} tableData={tableData} />
    </div>
  );
};

export default components;
