/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./node_modules/flowbite-react/**/*.js",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./index.html",
     "./src/**/*.{js,jsx,tsx}",
  ],
  mode: 'jit',
  theme: {
    screens: {
      tablet: '768px',
      smLaptop: '1200px',
      bgLaptop: '1440px',
      Desktop: '1680px',
    },
    extend: {
      colors: {
        brightRed: 'hsl(12, 88%, 59%)',
        brightRedLight: 'hsl(12, 88%, 69%)',
        brightRedSupLight: 'hsl(12, 88%, 95%)',
      },
      boxShadow :{
        sm : '0px 2px 4px 0px rgba(11, 10, , 0.15)'
      },
      fontSize: {
        txt14 : ['14px', { lineHeight : '24px', letterSpacing : '-0.03em' }],
        txt16 : ['16px', { lineHeight : '28px', letterSpacing : '-0.03em' }],
      },
      fontFamily :{
        poppins: ['Poppins', 'sans-serif'] // className="font-Poppins"
      },
    },
  },
  plugins: [require('flowbite/plugin')]
}
